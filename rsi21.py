#!/usr/bin/python
# -*- coding: utf-8
''' ИСТОРИЯ:
        1) Исправляема вход по фильтру. 
        '''
### --------------------------------------------------------------------------
import configparser
config = configparser.ConfigParser()
config.read('settings.ini') 
menu_config = config.get('Settings', "menu")
name_prog   = config.get('Settings', "iz_name_prog")
ver_prog    = config.get('Settings', "iz_name_ver")
### --------------------------------------------------------------------------
import iz_main
import iz_menu
start_param ={'name_prog':name_prog,'ver_prog':ver_prog,'namebot':'@main'}
iz_main.start_programm (start_param)
### --------------------------------------------------------------------------
import optparse
parser =  optparse.OptionParser(version='1.0',description='Шаблон')
parser.add_option('-m','--menu', type='string', dest='menu', help='Меню')
parser.add_option('-c','--coin', type='string', dest='coin', help='Монета')
(options, args) = parser.parse_args()
### --------------------------------------------------------------------------
        
from threading import Thread        
import random     
import time   
import datetime
import iz_func        
import iz_main
import iz_color

class ask_price(Thread):
    def __init__(self):
        Thread.__init__(self)        
        import time
        self.piar     = []   
        self.timeV = time.time()        
                                
    def run(self):
        import requests
        import json
        ask = 0
        bid = 0
        while 1==1:
            markets= iz_func.get_market_q ()
            for market in markets:            
                url = 'http://api3.cryptorobotics.io:55111/public/tickers/'+market+''
                if 1==1:
                    xtime = 10
                    lb = 'yes' 
                    while lb == 'yes':
                        try: 
                            r = requests.get(url)        
                            response = json.loads(r.text)['return']
                            lb = 'no'
                        except Exception as e:
                            print ('[-] Ошибка запроса Ticker. Ждем повтора через {} сек'.format(xtime))
                            print ('[!]',str(e))
                            xtime = xtime + 10
                            time.sleep(xtime)
                            if xtime > 60:
                                lb = 'no'  
                    for line in response:    
                        lb = 'no'
                        lb_ask = 0
                        lb_bid = 0
                        for l in self.piar:
                            if l[0] == market and l[1] == line :
                                lb = 'yes'         
                                lb_ask = l[2]
                                lb_bid = l[3]        
                        if lb == 'no':
                            self.piar.append([market,line,response[line]['ask'],response[line]['bid']])
                        else:
                            if response[line]['ask'] == lb_ask and response[line]['bid'] == lb_bid:
                                pass
                            else: 
                                if line == 'GAS/ETH':                            
                                    print  ('[-] {} Замена стоимости'.format (market) ,line,' ask {} - {}, bid {} - {} '.format (response[line]['ask'],lb_ask,response[line]['bid'],lb_bid))                                
                                s_list = []
                                for l in self.piar:
                                    if l[0] == market and l[1] == line :
                                        s_list.append([l[0],l[1],response[line]['ask'],response[line]['bid']])
                                    else:    
                                        s_list.append([l[0],l[1],l[2],l[3]])
                                self.piar = []        
                                self.piar = s_list
            timeK = time.time()          
            if timeK - self.timeV > 60*60*1:
                print ('[+] Выход по таймеру')
                exit (0)
            time.sleep(5)


def history(h,price_ask_bid):
    import json
    import requests
    print ('[+] Старт программы исторической хроники')
    respons = iz_func.doQueryRobots ()    
    ttame_to   = int(time.time())
    ttame_from = 1535760000   
    for number in range(0,10):    
        print('[+] Время смещения:',number)     
        ttame_from_l = ttame_from + number*60*60
        ttame_to_l   = ttame_from_l + 60*60*30        
        if 1539561600 < ttame_from:
            print ('[+] Конец процедуры')
            exit (0)        
        for line in respons:
            config,robotBuyMAstoploss,user_piar,robotBuyMAlot,exchange_user,user_id,bot_id,timeframe,start_param,demo_param,lots_param,stoplossAdditional = iz_func.getuserinfo (line)
            piars = json.loads(user_piar)
            exchange = exchange_user
            for piar in piars:
                print ('[+] {} {}'.format(exchange,piar))
                sim01,sim02 = iz_func.get_sim (piar)    
                url = 'http://185.220.34.177:55222/history/hour/'+exchange+'/'+sim01+'/'+sim02+'/'+str(ttame_to_l)+'/'+str(ttame_from_l)+''
                r = requests.get(url)
                podpiska = datetime.datetime.fromtimestamp(int(ttame_to_l)).strftime('%Y-%m-%d %H:%M:%S')
                print ('[+] {},{} - {}'.format(number,podpiska,r.text))
                if h == '6': 
                    close_1,ARI_t_1 = iz_func.get_data_1(exchange,sim01,sim02,30)
                    price_ask_bid = close_1[0]
                close_1 = close_1
                ARI_t_1 = ARI_t_1          
                if str(close_1).find ('Error') != -1: 
                    output_1 = close_1
                else:
                    output_1 = iz_func.rsitalib (close_1,14)
                    rsa_30_1  = output_1[30]
                    rsa_29_1  = output_1[29] 
                    rsa_28_1  = output_1[28]
                    rsa_27_1  = output_1[27]             
                    ARI_30_1  = ARI_t_1[30]
                    ARI_29_1  = ARI_t_1[29]
                    ARI_28_1  = ARI_t_1[28]
                    ARI_27_1  = ARI_t_1[27]            
                    signal = get_signal (rsa_30_1,rsa_29_1)
                    if signal == 'Вход 30':
                            good_signal = signal_good(exchange,piar,h,signal,price_ask_bid)
                            good_signal.start() 
                    if signal == 'Вход 50':
                            good_signal = signal_good(exchange,piar,h,signal,price_ask_bid)
                            good_signal.start() 
                    if signal == 'Выход 50':
                            good_signal = signal_good(exchange,piar,h,signal,price_ask_bid)
                            good_signal.start() 
                    if signal == 'Выход 57':
                            good_signal = signal_good(exchange,piar,h,signal,price_ask_bid)
                            good_signal.start()                  
    
def get_ask_and_bid ():
    print ('[+] Запускаем опрос ASK')
    price_ask_bid = ask_price()
    price_ask_bid.start() 
    return price_ask_bid   
    

def start_stop_loss(price_ask_bid): 
    import requests
    import json       
    import time    
    respons = iz_func.doQueryRobots ()   
    for line in respons:  
        stop_los_o = q_stop_los (line)       
        stop_los_o.start()
        time.sleep(2)


class q_stop_los(Thread):
    def __init__(self,line):
        Thread.__init__(self)        
        self.line = line
        
    def run(self):
        import json
        line = self.line
        config,robotBuyMAstoploss,user_piar,robotBuyMAlot,exchange_user,user_id,bot_id,timeframe,start_param,demo_param,lots_param,stoplossAdditional = iz_func.getuserinfo (line)
        robotBuyMAtakeprofit = json.loads(config)['robotBuyMAtakeprofit']
        iz_main.stop_loss (bot_id,user_id,exchange_user,robotBuyMAstoploss,demo_param,price_ask_bid,stoplossAdditional,robotBuyMAtakeprofit)


def print_ls (exchange,piar,h,message): 
    user_id = ''
    print ('[+] {:10s}{:10s} {:10s} {:10s} {:5s} {} '.format(iz_func.tm (),exchange,piar,str(user_id),h,message))     
    
 
def get_signal (rsa_30_1,rsa_29_1):     
    vhod  = 'нет сигнала'
    vihod = 'нет сигнала'
    if rsa_30_1 > 0  and rsa_29_1 > 0:       
        vhod  = 'нет входа'
        vihod = 'нет выхода'
        if rsa_30_1 < 35 and rsa_30_1 > 30 and rsa_29_1 < 30:
            vhod_30 = 'Вход 30'
            vhod    = 'Вход 30'
        if rsa_30_1 < 55 and rsa_30_1 > 50 and rsa_29_1 < 50 and rsa_29_1 > 39:
            vhod_50 = 'Вход 50'
            vhod    = 'Вход 50'
        if rsa_30_1 > 50:
            exit50   = 'Выход 50'
            vihod    = 'Выход 50'           
        if rsa_30_1 > 57:
            exit57   = 'Выход 57'
            vihod    = 'Выход 57'
    return vhod,vihod


class signal_good(Thread):
    def __init__(self,exchange,piar,h,signal,price_ask_bid):
        Thread.__init__(self)
        self.exchange = exchange
        self.piar     = piar
        self.h        = h        
        self.signal   = signal 
        self.price_ask_bid   = price_ask_bid        
        
        
    def run(self):
        import json
        respons = config_setting
        for line in respons:
            config,robotBuyMAstoploss,user_piar,robotBuyMAlot,exchange_user,user_id,bot_id,timeframe,start_param,demo_param,lots_param,stoplossAdditional = iz_func.getuserinfo (line)
            robotBuyMAtakeprofit = json.loads(config)['robotBuyMAtakeprofit']
            lots_param  = float(lots_param)
            start_param = float(start_param)
            if str(timeframe) == "1h" and str(self.h) == "1":
                answer_list = iz_func.in_list (exchange_user,user_piar,self.exchange,self.piar)            
                if answer_list == 'в списке': 
                    if start_param == 2: 
                        if self.signal == 'Вход 30':
                            vhod_30  = 'Вход 30'
                            vhod_50  = ''    
                            iz_main.input_marg (bot_id,user_id,self.exchange,self.piar,robotBuyMAlot,start_param,demo_param,lots_param,vhod_30,vhod_50,self.h,self.price_ask_bid,self.h) 
                        if self.signal == 'Вход 50':
                            vhod_30  = ''
                            vhod_50  = 'Вход 50'
                            iz_main.input_marg (bot_id,user_id,self.exchange,self.piar,robotBuyMAlot,start_param,demo_param,lots_param,vhod_30,vhod_50,self.h,self.price_ask_bid,self.h)                 
                    if start_param == 2 or start_param == 1:
                        print ("[&] Проверка на таймпрофит",robotBuyMAtakeprofit)
                        
                        if float(robotBuyMAtakeprofit) == 0:
                            if self.signal == 'Выход 50':
                                exit50  = 'Выход 50'
                                exit57  = ''
                                iz_main.exit_marg (bot_id,user_id,self.exchange,self.piar,start_param,demo_param,lots_param,exit50,exit57,self.h,self.price_ask_bid,self.h)
                            if self.signal == 'Выход 57':
                                exit50  = ''
                                exit57  = 'Выход 57'
                                iz_main.exit_marg (bot_id,user_id,self.exchange,self.piar,start_param,demo_param,lots_param,exit50,exit57,self.h,self.price_ask_bid,self.h)                        
                        else:
                            print ("[+] Ждем хорошего профита")    
                                
                                
                    if start_param != 2 and start_param != 1: 
                        pass      
                else:
                    pass
            else:
                pass
                
                
class RSI_PE(Thread):    
    def __init__(self,exchange,piar,h,price_ask_bid):
        Thread.__init__(self)
        self.exchange = exchange
        self.piar     = piar
        self.h        = h 
        self.price_ask_bid = price_ask_bid
        self.output_1 = 'нет данных'
        self.close_1  = 'нет данных'
        self.ARI_t_1  = 'нет данных'        
        self.signal   = 'нет сигнала'  

        
    def run(self):
        sim01,sim02 = iz_func.get_sim (self.piar) 
        if h == '1': close_1,ARI_t_1 = iz_func.get_data_1(self.exchange,sim01,sim02,30)   
        if h == '4': close_1,ARI_t_1 = iz_func.get_data_4(self.exchange,sim01,sim02,30)
        self.close_1 = close_1
        self.ARI_t_1 = ARI_t_1          
        if str(close_1).find ('Error') != -1: 
            self.output_1 = close_1
        else:
            self.output_1 = iz_func.rsitalib (self.close_1,14)
            rsa_30_1 = self.output_1[30]
            rsa_29_1 = self.output_1[29] 
            rsa_28_1 = self.output_1[28]
            rsa_27_1 = self.output_1[27]             
            ARI_30_1  = self.ARI_t_1[30]
            ARI_29_1  = self.ARI_t_1[29]
            ARI_28_1  = self.ARI_t_1[28]
            ARI_27_1  = self.ARI_t_1[27]      
            vhod,vihod = get_signal (rsa_30_1,rsa_29_1)
            self.signal = [vhod,vihod]
            if vhod == 'Вход 30':
                good_signal = signal_good(self.exchange,self.piar,self.h,vhod,self.price_ask_bid)
                good_signal.start() 
            if vhod == 'Вход 50':
                good_signal = signal_good(self.exchange,self.piar,self.h,vhod,self.price_ask_bid)
                good_signal.start() 
            if vihod == 'Выход 50':
                good_signal = signal_good(self.exchange,self.piar,self.h,vihod,self.price_ask_bid)
                good_signal.start() 
            if vihod == 'Выход 57':
                good_signal = signal_good(self.exchange,self.piar,self.h,vihod,self.price_ask_bid)
                good_signal.start()  

            
def create_market (h,price_ask_bid):
    markets= iz_func.get_market ()
    for market in markets:
        print ("[+]",market[0])
        start_market = RuningMarket(market,h,price_ask_bid)
        start_market.start()


class RuningMarket(Thread):
    def __init__(self, market,h,price_ask_bid):
        Thread.__init__(self)
        self.market = market
        self.h      = h   
        self.price_ask_bid = price_ask_bid        
    def run(self):      
        market = self.market
        h      = self.h   
        start_market_h (market,h,price_ask_bid)

        
def start_market_h (market,h,price_ask_bid):    
    piars = market[1] 
    random.shuffle(piars)
    RSIs = [] 
    exchange = market[0]
    for piar in piars:                     
        if piar.find('BTC') != -1 or piar.find('ETH') != -1 or piar.find('USDT') != -1:
            R =  RSI_PE(exchange,piar,h,price_ask_bid)
            R.start ()   
            time.sleep(1)                
        else:
            pass
                
                        
if __name__ == "__main__":
    main_title =['Главное меню программы']
    main_menu  =[['Демонстрация','demo'],
                 ['API','API']]
    main_param ={'parser':options.menu,'config':menu_config}
    respons = iz_main.menu (main_title,main_menu,main_param)
    h = respons
    print ('[+] ПРОГРАММА DСС',respons)    
    import datetime
    b = datetime.datetime.now()
    if h == '6':
        price_ask_bid = ''
        history(h,price_ask_bid)
        exit (0)
    price_ask_bid = ''    
    if h == '6':
        get_ask_and_bid ()
    if h == '5':
        start_stop_loss(price_ask_bid)
    if h == '1' or h == '2' or h == '3' or h == '4': 
        iz_func.log_error (0,0,'Началась работа программы  по поиску сигналов','info')
        config_setting = iz_func.doQueryRobots ()
        create_market(h,price_ask_bid)
        iz_func.log_error (0,0,'Закончилась работа программы по поиску сигналов','info') 

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    